// Importamos el paquete express
import express from "express";

import * as productos from "./productos";

// Instanciamos una app de express
const app = express();
app.use(express.json());

// Definimos una ruta y su handler correspondiente
app.get("/", function (request, response) {
  response.send("¡Bienvenidos a Express!");
});

// Obtiene todos los productos
app.get("/productos", function (request, response) {
  response.send(productos.getStock());
});

// Obtiene un producto dado su "id" (indice dentro del arreglo), devuelve un error en caso que no exista
app.get("/productos/:id", function (request, response) {
  let producto = productos.getProducto(+request.params.id);

  if (!producto)
    return response.send({
      error: "Producto no encontrado",
    });

  return response.send(producto);
});

// Agrega un producto al final del arreglo
app.post("/productos", function (request, response) {
  let producto = productos.storeProductos(request.body);

  if (!producto)
    return response.send({
      error: "Producto no agregado",
    });

  return response.send(producto);
});

// Edita un producto dado su "id" (indice dentro del arreglo), devuelve un error en caso que no exista o no pueda editarlo
app.put("/productos/:id", function (request, response) {
  let producto = productos.modifyProducto(request.body, +request.params.id);

  if (!producto)
    return response.send({
      error: "Producto no editado",
    });

  return response.send(producto);
});

// Elimna un producto dado su "id" (indice dentro del arreglo), devuelve un error en caso que no exista o no pueda eliminarlo
app.delete(`/productos/:id`, function (request, response) {
  let producto = productos.deleteProductos(+request.params.id);

  if (!producto)
    return response.send({
      error: "Producto no eliminado",
    });

  return response.send(producto);
});

// Ponemos a escuchar nuestra app de express
app.listen(3000, function () {
  console.info("Servidor escuchando en http://localhost:3000");
});
