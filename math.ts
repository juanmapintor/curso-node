//Función con nombre o nombrada
export function suma(x: number, y: number): number {
  return x + y;
}
//Función flecha
export const sumaFlecha = (x: number, y: number): number => {
  return x + y;
};
//Función con parámetros opcionales
export const opcionalSuma = (x: number, y?: number): number => {
  if (!y) return x;
  return x + y;
};

//Otra opción es agregarle un valor por defecto
export const opcionalSuma2 = (x: number, y: number = 0): number => {
  return x + y;
};

//Función flecha de resta
export const restaFlecha = (x: number, y: number): number => {
  return x - y;
};

//Función nombrada de división
export function division(x: number, y: number): number {
  return x / y;
}