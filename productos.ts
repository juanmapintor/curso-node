// productos.ts
// Creando una lista de productos
export interface Producto {
  nombre: string;
  precio: number;
  stock: number;
}
//
const Producto1: Producto = {
  nombre: "Yogurt",
  precio: 100,
  stock: 13,
};
const Producto2: Producto = {
  nombre: "Paquete de Galletas",
  precio: 89.9,
  stock: 40,
};
const Producto3: Producto = {
  nombre: "Lata Coca-Cola",
  precio: 80.5,
  stock: 0,
};
const Producto4: Producto = {
  nombre: "Alfajor",
  stock: 12,
  precio: 40,
};

let productos: Array<Producto> = [Producto1, Producto2, Producto3, Producto4];

export function getStock() {
  return productos;
}

export function getProducto(indexProducto: number): Producto | null {
  if (!productos[indexProducto]) return null;
  return productos[indexProducto];
}

export function storeProductos(body: any): Producto | null {
  if(!body.hasOwnProperty('nombre') || !body.hasOwnProperty('stock') || !body.hasOwnProperty('precio')) return null;
  
  let producto: Producto = {
    nombre: body.nombre,
    stock: body.stock,
    precio: body.precio,
  };

  if(productos.push(producto) <= 0) return null;

  return producto;
}

export function modifyProducto(
  body: any,
  indexProducto: number
): Producto | null {
  if (!productos[indexProducto]) return null;

  if (body.nombre) productos[indexProducto].nombre = body.nombre;
  if (body.stock) productos[indexProducto].stock = body.stock;
  if (body.precio) productos[indexProducto].precio = body.precio;

  return productos[indexProducto];
}

export function deleteProductos(indice: number) {
  if(indice >= productos.length) return null;
  return productos.splice(indice)[0];
}
